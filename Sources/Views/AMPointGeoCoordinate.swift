class AMPointGeoCoordinate : ARGeoCoordinate {
    let point : AMPoint
    init(point : AMPoint) {
        self.point = point
        
    }
    class func coordinate(withPoint point : AMPoint, fromOrigin origin : CLLocation? = nil) -> AMPointGeoCoordinate {
        let coord = AMPointGeoCoordinate(point: point)
        if let origin = origin {
            coord.calibrate(usingOrigin: origin)
        }
        return coord
    }
}
