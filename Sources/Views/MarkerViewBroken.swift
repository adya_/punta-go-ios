@objc protocol MarkerViewDelegate : NSObjectProtocol {
    @objc func didTouchMarkerView(_ markerView : MarkerViewBroken)
}

class MarkerViewBroken : UIImageView {
    var coordinate : AMPointGeoCoordinate? = nil
    var delegate : MarkerViewDelegate? = nil
    
    fileprivate var isMaximized : Bool = false
    
    init(withCoordinate coordinate : AMPointGeoCoordinate, delegate : MarkerViewDelegate? = nil) {
        super.init(frame: CGRect.zero)
        self.coordinate = coordinate
        self.delegate = delegate
        self.isUserInteractionEnabled = true
        if let path = AMStorage.shared().path(toPointImage: coordinate.point),
            let image = UIImage(contentsOfFile: path) {
            self.image = image
        } else {
            self.image = UIImage(named: "Placeholder")
        }
        self.contentMode = .scaleAspectFit
        self.sizeToFit()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        tap.numberOfTapsRequired = 2
        self.addGestureRecognizer(tap)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc fileprivate func imageTapped(_ gesture : UIGestureRecognizer) {
        if isMaximized {
            self.sizeToFit()
            self.sizeToFit()
        } else if let bounds = self.superview?.bounds {
            self.frame = bounds
        } else {
            print("Failed to maximize MarkerView")
        }
    }
}
