class ImageAnnotationView : ARAnnotationView {
    
    private var ivPoint : UIImageView!

    override func initialize() {
        self.ivPoint = UIImageView()
        self.ivPoint.contentMode = .ScaleAspectFit
        var constraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-[img]-|", options: [], metrics: nil, views: ["img" : self.ivPoint])
        constraints += NSLayoutConstraint.constraintsWithVisualFormat("H:|-[img]-|", options: [], metrics: nil, views: ["img" : self.ivPoint])
        constraints.forEach{$0.active = true}
    }
    
    override func bindUi() {
        guard let annotation = self.annotation as? AMPointAnnotation else {
            print("Annotation wasn't associated with any point.")
            return
        }
        if let path = AMStorage.sharedStorage().pathToPointImage(annotation.point),
            image = UIImage(contentsOfFile: path) {
            self.ivPoint.image = image
        } else {
            print("Image for point '\(annotation.point.name)' was not found.")
            self.ivPoint.image = UIImage(named: "Placeholder")
        }
    }
}
