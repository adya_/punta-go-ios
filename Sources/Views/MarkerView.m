#import "MarkerView.h"

#import "ARGeoCoordinate.h"
#import "AMPoint.h"
#import "AMStorage.h"
@interface MarkerView ()

@property (nonatomic, strong) UIImageView *imagePoint;

@end


@implementation MarkerView {
    BOOL isMaximized;
}

- (id)initWithFrame:(CGRect)frame
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    self = [super initWithFrame: CGRectMake(-screenBounds.size.width,
                                            -screenBounds.size.height,
                                            screenBounds.size.width * 2 + 50,
                                            screenBounds.size.height * 2)];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoordinate:(ARGeoCoordinate *)coordinate forPoint:(AMPoint*) point delegate:(id<MarkerViewDelegate>)delegate {
    if((self = [self initWithFrame: CGRectZero])) {
        _coordinate = coordinate;
        _delegate = delegate;
        
        [self setUserInteractionEnabled: YES];
        NSString* path = [[AMStorage sharedStorage] pathToPointImage: point];
        self.imagePoint = [[UIImageView alloc] initWithFrame: self.frame];

        self.imagePoint.image = [UIImage imageWithContentsOfFile: path];
        self.imagePoint.contentMode = UIViewContentModeScaleAspectFit;
//        [self.imagePoint sizeToFit];
//        [self sizeToFit];
//
//        UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
//        tap.numberOfTapsRequired = 2;
//        
//        [self.imagePoint addGestureRecognizer:tap];
        [self addSubview:self.imagePoint];
    }
    
    return self;
}

//- (void) imageTapped: (UIGestureRecognizer*) gesture {
//    if (isMaximized) {
//        [self minimize];
//    } else {
//        [self maximize];
//    }
//    isMaximized = !isMaximized;
//}
//
//-(void) minimize {
//    [self.imagePoint sizeToFit];
//}
//
//-(void) maximize {
//    self.imagePoint.frame = self.superview.bounds;
//}

@end
