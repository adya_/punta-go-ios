class AMPointAnnotation : ARAnnotation {
    
    let point : AMPoint
    
    init(point : AMPoint) {
        self.point = point
        super.init()
        self.title = point.name
        self.location = point.arLocation
    }
}