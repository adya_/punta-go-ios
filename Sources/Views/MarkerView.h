#import <UIKit/UIKit.h>

@class AMPoint;
@class ARGeoCoordinate;
@protocol MarkerViewDelegate;

@interface MarkerView : UIView

@property (nonatomic, strong) ARGeoCoordinate *coordinate;
@property (assign, nonatomic) id <MarkerViewDelegate> delegate;
- (id)initWithCoordinate:(ARGeoCoordinate *)coordinate forPoint:(AMPoint*) point delegate:(id<MarkerViewDelegate>)delegate;

@end

@protocol MarkerViewDelegate <NSObject>

- (void)didTouchMarkerView:(MarkerView *)markerView;

@end
