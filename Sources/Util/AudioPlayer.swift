import AVFoundation

@objc protocol AudioPlayerDelegate : NSObjectProtocol {
    @objc func audioPlayerDidFinishPlaying()
}

class AudioPlayer : NSObject {
    static let shared : AudioPlayer = AudioPlayer()
    
    fileprivate var player : AVAudioPlayer?
    var delegate : AudioPlayerDelegate?
    
    func play(_ path : String) {
        let url = URL(fileURLWithPath: path)
        if self.playing {
            self.player?.stop()
        }
        self.player = try? AVAudioPlayer(contentsOf: url)
        self.player?.delegate = self
        self.player?.play()
        print("Plying sound: '\(url.lastPathComponent ?? "Unknown")'")
    }
    
    func stop() {
        if self.playing {
            self.player?.stop()
            self.player = nil
        }
    }
        
    func pause() {
        if self.playing {
            self.player?.pause()
        }
    }
    
    func resume() {
        if self.paused {
            self.player?.play()
        }
    }
    
    var playing : Bool {
        return self.player?.isPlaying ?? false
    }
    
    var paused : Bool {
        return self.player != nil && !self.playing
    }
    
    deinit {
        self.player?.stop()
    }
}

extension AudioPlayer : AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if flag {
            self.player = nil
        }
        self.delegate?.audioPlayerDidFinishPlaying()
    }
}
