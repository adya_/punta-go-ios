#import <Foundation/Foundation.h>

@class AMZone;

typedef void(^AMZonesLoaderCompletionHandler)(BOOL isSuccess,  NSString* _Nullable  version,  NSArray<AMZone*>* _Nullable  response);

@interface AMZonesLoader : NSObject
+(void) loadZonesForIdiom:(nonnull NSString*) idiom withCompletionHandler:(nullable AMZonesLoaderCompletionHandler) completion;
@end
