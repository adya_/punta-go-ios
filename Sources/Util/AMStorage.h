#import <Foundation/Foundation.h>

@class AMZone;
@class AMPoint;

@protocol AMStorageLoadingDelegate <NSObject>

@optional
-(void) storageDidLoadHelp:(nullable NSString*) help;
-(void) storageDidFinishWithZones:(nullable NSArray<AMZone*>*) zones updated:(BOOL) isUpdated;
-(void) storageWillLoadFiles:(NSInteger) total;
-(void) storageDidLoadFile:(nonnull NSString*) filepath forPoint:(nullable AMPoint*) point progress:(NSInteger) index of:(NSInteger) total;

-(void) storageFailedLoadingFile:(nonnull NSString*) filepath;
-(void) storageDidFailWithError:(nonnull NSString*) error;
@end

@interface AMStorage : NSObject
@property (readonly)  NSArray<AMZone*>* _Nullable zones;
@property  NSString* _Nullable idiom;
@property  NSString* _Nonnull help;
@property (readonly)  NSArray<NSString*>* _Nonnull availableIdioms;

@property (assign) id<AMStorageLoadingDelegate> _Nullable delegate;

+(nonnull instancetype) sharedStorage;

-(void) updateStorage;
-(void) updateStorage:(BOOL) forced;

-(nullable NSString*) pathToZoneAudio:(nonnull AMZone*) zone;
-(nullable NSString*) pathToPointAudio:(nonnull AMPoint*) point;
-(nullable NSString*) pathToPointImage:(nonnull AMPoint*) point;
@end
