#import <CoreLocation/CoreLocation.h>
#import "AMZonesLoader.h"
#import "AMPoint.h"
#import "AMZone.h"

static const NSString* ServiceUrl = @"http://52.20.198.95/acompasados/index/gateway";
//@"http://xenioit.com/xenio/backend/acompasadosnew/index/gateway";

@implementation AMZonesLoader

+(void) loadZonesForIdiom:(NSString*) idiom withCompletionHandler:(AMZonesLoaderCompletionHandler) completion {
    NSString* paramUrl = [NSString stringWithFormat: @"%@?atype=getZonalPoints&project=%@", ServiceUrl, idiom];
    NSURL *url = [NSURL URLWithString: paramUrl];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    NSLog(@"Requesting: %@", paramUrl);
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        if (data.length > 0 && !connectionError)
        {
            NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:0
                                                                   error:NULL];
            NSLog(@"Received:\n%@", dict);
            if (completion) completion(YES, dict[@"API_VERSION"], [self buildZones:dict[@"zones"]]);
        }
        else if(completion) completion(NO, nil, nil);
    }];
}

+(NSArray<AMZone*>*) buildZones:(NSArray<NSDictionary*>*) dicts{
    NSMutableArray* points = [NSMutableArray new];
    for (NSDictionary* dic in dicts) {
        [points addObject:[self buildZone:dic]];
    }
    return [NSArray arrayWithArray:points];
}

+(AMZone*) buildZone:(NSDictionary*) dic {
    AMZone* zone = [AMZone new];
    zone.zoneID = dic[@"zoneID"];
    zone.name = dic[@"zoneName"];
    zone.audio = [[NSURL alloc] initWithString: dic[@"zoneAUDIO"]];
    zone.points = [self buildPoints:dic[@"points"]];
    return zone;
}

+(NSArray<AMPoint*>*) buildPoints:(NSArray<NSDictionary*>*) dicts{
    NSMutableArray* points = [NSMutableArray new];
    for (NSDictionary* dic in dicts) {
        [points addObject:[self buildPoint:dic]];
    }
    return [NSArray arrayWithArray:points];
}

+(AMPoint*) buildPoint:(NSDictionary*) dic{
    AMPoint* pt = [AMPoint new];
    pt.pointID = [dic[@"pointID"] intValue];
    pt.isAudioPoint = [dic[@"Audio_ar"] boolValue];
    NSNumber* lat;
    NSNumber* lon;
    if (pt.isAudioPoint) {
        lat = dic[@"loclat"];
        lon = dic[@"loclng"];
    }
    else{
        lat = dic[@"loclat_2"];
        lon = dic[@"loclng_2"];
        pt.arLocation = [[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[lon doubleValue]];
        lat = dic[@"loclat_3"];
        lon = dic[@"loclng_3"];
    }
    pt.location = [[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[lon doubleValue]];
    pt.distance = [dic[@"distance"] floatValue];
    pt.name = dic[@"pointName"];
    pt.image = [[NSURL alloc] initWithString: dic[@"pointIMAGE"]];
    pt.audio = [[NSURL alloc] initWithString: dic[@"pointAUDIO"]];
    return pt;
}
@end
