@interface Downloader : NSObject
-(BOOL) downloadDataFromURL:(NSURL*) urlString toFile:(NSString*) filePath;
@end
