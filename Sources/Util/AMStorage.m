#import "AMStorage.h"
#import "TSLocalStorage.h"
#import "AMZonesLoader.h"
#import "AMZone.h"
#import "Downloader.h"
#import "AMPoint.h"

NSString* const kAMStorageVersion = @"AMStorageVersion";
NSString* const kAMStorageZones = @"AMStrageZones";
NSString* const kAMStorageIdiom = @"AMStorageIdiom";
NSString* const kAMStorageHelp = @"AMStorageHelp";

@implementation AMStorage{
    NSString* _version;
}

@synthesize delegate;
@synthesize idiom;
@synthesize help;

+(instancetype) sharedStorage{
    static AMStorage* instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [AMStorage new];
        [instance loadLocal];
    });
    return instance;
}

-(void) updateStorage {
    [self updateStorage: NO];
}

-(void) updateStorage:(BOOL)forced {
    [self loadRemote: forced];
}

-(NSArray*) availableIdioms {
    return @[@"PDE-Espanol", @"PDE-Ingles", @"PDE-Portugues"];
}

-(NSString*) defaultHelp {
    NSString* path = [[NSBundle mainBundle] pathForResource:@"help"
                                    ofType:@"txt"];
    return [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
}

-(void) loadLocal{
    _version = [TSLocalStorage loadObjectForKey: kAMStorageVersion];
    idiom = [TSLocalStorage loadObjectForKey: kAMStorageIdiom];
    help = [TSLocalStorage loadObjectForKey:kAMStorageHelp];
    if (!help) {
        help = self.defaultHelp;
    }
    _zones = [NSKeyedUnarchiver unarchiveObjectWithData:[TSLocalStorage loadObjectForKey: kAMStorageZones]];
    NSLog(@"Storage loaded (Version: %@, Idiom: %@)", _version, idiom ? idiom : @"Undefined");
}

-(void) saveLocal{
    [TSLocalStorage saveObject:_version forKey: kAMStorageVersion];
    [TSLocalStorage saveObject:idiom forKey: kAMStorageIdiom];
    [TSLocalStorage saveObject:help forKey:kAMStorageHelp];
    [TSLocalStorage saveObject:[NSKeyedArchiver archivedDataWithRootObject:_zones] forKey: kAMStorageZones];
}

-(void) loadRemote:(BOOL) forced{
    if (!idiom) {
        NSLog(@"Idiom was not set.");
        if ([delegate respondsToSelector:@selector(storageDidFailWithError:)]){
            [delegate storageDidFailWithError:@"Failed to update."];
        }
        return;
    }
    [AMZonesLoader loadZonesForIdiom:idiom withCompletionHandler:^(BOOL isSuccess, NSString *version, NSArray<AMZone*> *response) {
        if (isSuccess) {
            _zones = response;
            if (forced || ![version isEqualToString:_version] || [self isMissingFiles]){
                _version = version;
                NSLog(@"Updating storage to %@ with idiom %@", _version, idiom);
                [self loadData];
            }
            else{
                NSLog(@"Storage is up-to-date.");
                if ([delegate respondsToSelector:@selector(storageDidFinishWithZones:updated:)]){
                    [delegate storageDidFinishWithZones:_zones updated:NO];
                }
                [self saveLocal];
            }
        } else{
            NSLog(@"Failed to update.");
            if ([delegate respondsToSelector:@selector(storageDidFailWithError:)]){
                [delegate storageDidFailWithError:@"Failed to update."];
            }
        }
    }];

    [AMHelpLoader loadHelp:^(NSString* _help) {
        if (_help) {
            self.help = _help;
            NSLog(@"Help content loaded.");
            [self saveLocal];
        } else {
            NSLog(@"Failed to load help.");
        }
        
        if ([delegate respondsToSelector:@selector(storageDidLoadHelp:)]){
            [delegate storageDidLoadHelp: self.help];
        }
    }];
}

-(void) loadData {
    AMZone* zone = self.zones.firstObject;
    NSInteger total = zone.points.count + 1;
    if ([delegate respondsToSelector:@selector(storageWillLoadFiles:)])
        [delegate storageWillLoadFiles: total];
    Downloader *downloader = [Downloader new];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString* path = [self pathToZoneAudio: zone onlyIfExists:NO];
        NSString* root = [path stringByDeletingLastPathComponent];
        if (![[NSFileManager defaultManager] fileExistsAtPath: root]) {
            BOOL created = [[NSFileManager defaultManager] createDirectoryAtPath:root withIntermediateDirectories:YES attributes:nil error:nil];
            NSLog(@"%@ under '%@'", created ? @"New version created" : @"Failed to create new version", path);
        }
        NSLog(@"Downloading zone '%@' at '%@'", zone.name, zone.audio);
        BOOL res = [downloader downloadDataFromURL: zone.audio toFile: path];
        [self loadZonePointsAudio: zone];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (res){
                NSLog(@"Zone '%@' has been downloaded!", zone.name);
                if ([delegate respondsToSelector:@selector(storageDidLoadFile:forPoint:progress:of:)])
                    [delegate storageDidLoadFile: path forPoint:nil progress: 1 of: total];
            }
            else{
                NSLog(@"Failed to load zone '%@'", zone.name);
                if ([delegate respondsToSelector:@selector(storageFailedLoadingFile:)])
                    [delegate storageFailedLoadingFile: path];
            }
        });
    });
}

-(void) loadZonePointsAudio:(AMZone*) zone {
    NSInteger total = zone.points.count + 1;
    __block NSInteger progress = 0;
    dispatch_group_t group = dispatch_group_create();
    Downloader *downloader = [Downloader new];
    for (AMPoint *point in zone.points) {
        NSURL* url = point.isAudioPoint ? point.audio : point.image;
        NSString* path = point.isAudioPoint ? [self pathToPointAudio: point onlyIfExists:NO]
        : [self pathToPointImage: point onlyIfExists:NO];
        NSLog(@"Downloading point '%@' at '%@'", point.name, url);
        dispatch_group_enter(group);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            BOOL res = [downloader downloadDataFromURL: url toFile: path];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (res){
                    NSLog(@"Point '%@' has been downloaded!", point.name);
                    if ([delegate respondsToSelector:@selector(storageDidLoadFile:forPoint:progress:of:)])
                        [delegate storageDidLoadFile: path forPoint: point progress: ++progress of: total];
                }
                else{
                    NSLog(@"Failed to load point '%@'", zone.name);
                    if ([delegate respondsToSelector:@selector(storageFailedLoadingFile:)])
                        [delegate storageFailedLoadingFile: path];
                }
                
            });
            dispatch_group_leave(group);
        });
    }
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        NSLog(@"Storage updated.");
        if ([delegate respondsToSelector:@selector(storageDidFinishWithZones:updated:)]){
            [delegate storageDidFinishWithZones:_zones updated:YES];
        }
        [self saveLocal];
    });
}

-(BOOL) isMissingFiles {
    NSString* filepath = [self pathToRoot];
    return ![[NSFileManager defaultManager] fileExistsAtPath: filepath] ||
    ([[[NSFileManager defaultManager] contentsOfDirectoryAtPath:filepath error: nil] count] != self.zones.firstObject.points.count + 1);
}

-(NSString*) existingPath:(NSString*) path {
    return [[NSFileManager defaultManager] fileExistsAtPath: path] ? path : nil;
}

-(NSString*) pathToZoneAudio:(AMZone *)zone {
    return [self pathToZoneAudio: zone onlyIfExists: YES];
}

- (NSString*) pathToPointAudio:(AMPoint *)point {
    return [self pathToPointAudio: point onlyIfExists: YES];
}

-(NSString*) pathToPointImage:(AMPoint *)point {
    return [self pathToPointImage:point onlyIfExists: YES];
}

-(NSString*) pathToZoneAudio:(AMZone*) zone onlyIfExists: (BOOL) mustExist {
    NSString* path = [self rootedPath: [zone.name stringByAppendingPathExtension:@"mp3"]];
    return mustExist ? [self existingPath:path] : path;
}

-(NSString*) pathToPointAudio:(AMPoint*) point onlyIfExists: (BOOL) mustExist {
    NSString* path = [self rootedPath: [point.name stringByAppendingPathExtension:@"mp3"]];
    return mustExist ? [self existingPath:path] : path;
}

-(NSString*) pathToPointImage:(AMPoint*) point onlyIfExists: (BOOL) mustExist {
    NSString* path = [self rootedPath: [point.name stringByAppendingPathExtension:@"png"]];
    return mustExist ? [self existingPath:path] : path;
}

-(NSString*) pathToDocuments {
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    return documentsPath;
}

-(NSString*) pathToRoot {
    return [[[self pathToDocuments] stringByAppendingPathComponent:_version] stringByAppendingPathComponent: idiom];
}

-(NSString*) rootedPath: (NSString*) filename {
    return [[self pathToRoot] stringByAppendingPathComponent: filename];
}
@end
