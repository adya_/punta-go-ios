class AMHelpLoader : NSObject {
    typealias AMHelpCompletion = (_ help : String?) -> Void

    class func loadHelp(_ completion : AMHelpCompletion? = nil) {
        guard let url = URL(string: "http://http://52.20.198.95/acompasados/help.php") else {
            completion?(nil)
            return
        }
        let request = URLRequest(url: url)
        NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.main) { (response, data, error) in
            guard let data = data,
            let help = String(data: data, encoding: String.Encoding.utf8)
                else {
                completion?(nil)
                return
            }
            completion?(help)
        }
    }
}
