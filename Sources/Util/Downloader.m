#import "Downloader.h"

@implementation Downloader

-(BOOL)downloadDataFromURL:(NSURL*) url toFile:(NSString*) filePath {
    
    
    if ([[NSFileManager defaultManager] fileExistsAtPath: filePath]) {
        return YES;
    }
    NSData *data = [NSData dataWithContentsOfURL: url];
    NSError *error = nil;
    BOOL success = [data writeToFile: filePath options: NSDataWritingAtomic error: &error];
    if (error) {
        NSLog(@"Error: \n%@", error.userInfo.description);
    }
    return success;
}

@end
