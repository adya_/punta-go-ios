class MapViewController : PortraitViewController {
    @IBOutlet fileprivate var vMap : RMMapView!
    @IBOutlet fileprivate var lIdiom : UILabel!
    @IBOutlet weak fileprivate var bLocationAccess: UIButton!
    
    fileprivate var locationManager : CLLocationManager!
    fileprivate var regions : [CLRegion]?
    fileprivate var lastRegion : CLCircularRegion?
    fileprivate var points : [AMPoint]!
    fileprivate var currentPoint : AMPoint?
    fileprivate var markerUser : RMMarker!
    
    fileprivate func initializeMap() {
        self.vMap = RMMapView(frame: self.view.bounds)
        self.view.insertSubview(vMap, at: 0)
        
        let constraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[map]|", options: [], metrics: nil, views: ["map" : vMap]) +
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[map]|", options: [], metrics: nil, views: ["map" : vMap])
        NSLayoutConstraint.activate(constraints)
        
        self.vMap.markerManager
        self.vMap.delegate = self
        let mapSrc = RMDBMapSource(path: "mymap.sqlite")
        let _ = RMMapContents(view: self.vMap, tilesource: mapSrc)
        
        self.points?.forEach {
        guard let image = UIImage(named: $0.isAudioPoint ? "marker-blue" : "marker-red-withletter") else {
            print("Marker's image not found.")
            return
        }
//            let image : UIImage?
//            if $0.isImagePoint {
//                image = UIImage(contentsOfFile: AMStorage.sharedStorage().pathToPointImage($0)!)
//            } else{
//                image = UIImage(named: "marker-blue")
//            }
        
        let textColor = $0.isAudioPoint ? UIColor.blue : UIColor.orange
        let marker = RMMarker(uiImage: image, anchorPoint: CGPoint(x: 0.5, y: 1))
        marker?.textForegroundColor = textColor
        marker?.changeLabel(usingText: $0.name)
        self.vMap.contents.markerManager.addMarker(marker, atLatLong: $0.location.coordinate)
        }
        if let firstLocation = self.points?.first?.location.coordinate {
            self.vMap.move(toLatLong: firstLocation)
        }
        // Constrain our map so the user can only browse through our exported map tiles
        let bottom = mapSrc?.bottomRightOfCoverage()
        let top = mapSrc?.topLeftOfCoverage()
        self.vMap.setConstraintsSW( CLLocationCoordinate2D(latitude: (bottom?.latitude)!, longitude: (top?.longitude)!),
                                ne: CLLocationCoordinate2D(latitude: (top?.latitude)!, longitude: (bottom?.longitude)!))
        self.clearGeofences()
    }
    
    fileprivate func updateRegions() {
        self.regions?.forEach {
            self.locationManager.stopMonitoring(for: $0)
        }
        self.regions = self.points.filter{$0.isAudioPoint}.map {
            CLCircularRegion(center: $0.location.coordinate,
                radius: $0.distance > self.locationManager.maximumRegionMonitoringDistance ?
                    self.locationManager.maximumRegionMonitoringDistance :
                    $0.distance,
                identifier: $0.name)
        }
        self.regions?.forEach {
            self.locationManager.startMonitoring(for: $0)
        }
    }
    
    fileprivate func checkRegions() {
        if let region = self.regions?.flatMap ({ $0 as? CLCircularRegion}).filter ({
            let regionLocation = CLLocation(latitude: $0.center.latitude, longitude: $0.center.longitude)
            let distance = self.locationManager?.location.flatMap{regionLocation.distance(from: $0)} ?? -1.0
            return distance >= 0 && distance <= $0.radius
        }).first {
            if self.lastRegion?.identifier != region.identifier {
                self.regionChanged(region)
            }
        }
    }
    
    fileprivate func clearGeofences() {
        self.locationManager.monitoredRegions.forEach {
            self.locationManager.stopMonitoring(for: $0)
        }
    }
    
    
    
    fileprivate func checkLocationAvailability() -> Bool {
        guard CLLocationManager.locationServicesEnabled() else {
            self.showMessage("You need to enable Location Services in your settings in order to use Punta GO.")
            print("Location services are disabled on the device.")
            self.setLocationStatus("Enable Location Services")
            return false
        }
        
        guard CLLocationManager.isMonitoringAvailable(for: CLRegion.self) else {
            print("Region monitoring is not available on the device.")
            self.setLocationStatus("Location Services are unavailable on your device")
            return false
        }
        
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse, .authorizedAlways:
            self.setLocationStatus()
            return true
        case .notDetermined, .denied, .restricted:
            self.locationManager.requestWhenInUseAuthorization()
            if Bundle.main.object(forInfoDictionaryKey: "NSLocationWhenInUseUsageDescription") == nil {
                print("Info.plist does not contain NSLocationWhenInUseUsageDescription")
            }
            self.setLocationStatus("Allow app to use Location Services")
            return false
        }
    }
    
    fileprivate func setLocationStatus(_ message : String? = nil) {
        self.bLocationAccess.setTitle(message, for: UIControlState())
        self.bLocationAccess.isEnabled = message != nil
    }
    @IBAction func openSettings(_ sender: UIButton) {
        UIApplication.shared.openURL(URL(string : UIApplicationOpenSettingsURLString)!)
    }
    
    fileprivate func playPoint(_ point : AMPoint) {
        guard point.isAudioPoint, let path = AMStorage.shared().path(toPointAudio: point) else {
            print("Path for audio point '\(point.name ?? "Unknown")' doesn't exist.\(point.isImagePoint ? " Point doesn't have audio." : "")")
            return
        }
        if self.currentPoint != nil {
            AudioPlayer.shared.stop()
        }
        self.currentPoint = point
        AudioPlayer.shared.play(path)
        
    }
    
    fileprivate func regionChanged(_ region : CLCircularRegion) {
        guard let point = self.points.filter({$0.name == region.identifier}).first else {
            print("Point with name '\(region.identifier)' not found.")
            return
        }
        print("Entering point: '\(point.name)'")
        self.lastRegion = region
        self.vMap.move(toLatLong: region.center)
        self.playPoint(point)
    }
    
    fileprivate func showMessage(_ message : String){
        let alert = UIAlertView(title: "Punta GO", message: message, delegate: nil, cancelButtonTitle: "OK")
        alert.alertViewStyle = .default
        alert.show()
    }
    
    @objc fileprivate func resumed(_ notification : Notification) {
        self.checkLocationAvailability()
        self.locationManager?.startUpdatingLocation()
        self.checkRegions()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: - Navigation
extension MapViewController {
    @IBAction func unwindMap(_ segue : UIStoryboardSegue) {}
    func openAR() {
        self.performSegue(withIdentifier: "segARView", sender: self)
    }
}

// MARK: - Audio
extension MapViewController : AudioPlayerDelegate {
    func audioPlayerDidFinishPlaying() {
        print("Playback for point '\(self.currentPoint?.name ?? "Unknown")' finished.")
        self.currentPoint = nil
    }
}

// MARK: - View Life cycle
extension MapViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(resumed), name: NSNotification.Name(rawValue: kResumedNotification), object: nil)
        self.points = AMStorage.shared().zones?.flatMap{$0.points}
        self.locationManager = self.createLocationManager()
        self.initializeMap()
        self.updateRegions()
        self.lIdiom.text = AMStorage.shared().idiom.flatMap{"El idiom: \($0)"}
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIViewController.attemptRotationToDeviceOrientation()
        self.checkLocationAvailability()
        self.locationManager?.startUpdatingLocation()
        self.checkRegions()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.clearGeofences()
        self.locationManager?.stopUpdatingLocation()
     //   AudioPlayer.shared.stop()
    }
}

// MARK: - Initialization
extension MapViewController {
    func createLocationManager() -> CLLocationManager {
        let locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.distanceFilter = 3
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        return locationManager
    }
}

// MARK: - CLLocationManager delegate
extension MapViewController : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        guard let region = region as? CLCircularRegion else {
            print("Region is invalid.")
            return
        }
        
        let regionLocation = CLLocation(latitude: region.center.latitude, longitude: region.center.longitude)
        let distance = manager.location.flatMap{regionLocation.distance(from: $0)} ?? -1.0
        guard let point = self.points.filter({$0.name == region.identifier}).first else {
            print("Point with name '\(region.identifier)' not found.")
            return
        }
        print("Started monitoring '\(point.name ?? "Unknown")', distance to user: \(distance)")
            }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        guard let region = region as? CLCircularRegion else {
                print("Entered invalid region.")
                return
        }
        self.regionChanged(region)
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        guard let _ = self.points.filter({$0.name == region.identifier}).first else {
            print("Point with name '\(region.identifier)' not found.")
            return
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let latest = locations.last?.coordinate else {
            print("No locations available.")
            return
        }
        if (locations.count < 2) { // force to update location services message when starting updating locations.
            self.checkLocationAvailability()
        }
        if self.markerUser == nil {
            guard let image = UIImage(named: "marker-red") else {
                print("User marker's image not found.")
                return
            }
            self.markerUser = RMMarker(uiImage: image, anchorPoint: CGPoint(x: 0.5, y: 1))
            self.markerUser.textForegroundColor = UIColor.blue
            self.markerUser.changeLabel(usingText: "Yo")
            self.vMap.contents.markerManager.addMarker(self.markerUser, atLatLong: latest)
        }
        self.vMap.contents.markerManager.moveMarker(self.markerUser, atLatLon: latest)
        self.checkRegions()
    }
}

extension MapViewController : RMMapViewDelegate {
    func tap(on marker: RMMarker!, onMap map: RMMapView!) {
        guard let name = (marker?.label as? UILabel)?.text else {
            print("Couldn't retrieve name from marker.")
            return
        }
        guard let point = self.points.filter({$0.name == name}).first else {
            print("Point with name '\(name)' not found.")
            return
        }
        
        if point.isImagePoint {
            self.openAR()
        } else {
          //  self.playPoint(point)
        }
        
    }
}
