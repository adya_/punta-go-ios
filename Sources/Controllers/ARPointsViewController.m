#import "ARPointsViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "ARKit.h"

#import "AMZonesLoader.h"
#import "AMZone.h"
#import "AMPoint.h"
#import "AMStorage.h"

const int kInfoViewTag = 1001;
const int kMaxPointsDistance = 200;

@interface ARPointsViewController () <CLLocationManagerDelegate, MarkerViewDelegate, ARDelegate>

@property (nonatomic, strong) AugmentedRealityController *arController;
@property (nonatomic, strong) NSMutableArray<ARGeoCoordinate*> *geoLocations;
@property (nonatomic, strong) CLLocationManager *locationManager;
@end


@implementation ARPointsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self lazyInitAR];
    
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _locations = [AMStorage sharedStorage].zones.firstObject.imagePoints;
    [self startLocationManager];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self stopLocationManager];
}

-(UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

-(BOOL) shouldAutorotate {
    return YES;
}

-(void) lazyInitAR {
    if(!_arController) {
        _arController = [[AugmentedRealityController alloc] initWithView:self.view
                                                    parentViewController:self withDelgate: self];
    }
    [_arController setMinimumScaleFactor:0.5];
    [_arController setScaleViewsBasedOnDistance:YES];
    [_arController setRotateViewsBasedOnPerspective:YES];
    [_arController setDebugMode:NO];
}

-(void) lazyInitLocationManager{
    if (!_locationManager){
        _locationManager = [CLLocationManager new];
        [_locationManager setDelegate:self];
        [_locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
        [_locationManager requestWhenInUseAuthorization];
    }
}

-(void) startLocationManager{
    [self lazyInitLocationManager];
    [_locationManager startUpdatingLocation];
}

-(void) stopLocationManager{
    if (_locationManager)
        [_locationManager stopUpdatingLocation];
}

#pragma mark - Actions

#pragma mark - CLLocationManager Delegate

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *lastLocation = [locations lastObject];
    
    CLLocationAccuracy accuracy = [lastLocation horizontalAccuracy];
    
    NSLog(@"Received location %@ with accuracy %f", lastLocation, accuracy);
    [self filterForDistance:lastLocation];
}

-(void)filterForDistance:(CLLocation *)location {
    BOOL containsAll = YES;
    NSMutableArray *locationsCopy = [NSMutableArray array];
    for (AMPoint* point in _locations) {
        CLLocationDistance distance = [location distanceFromLocation:point.location];
        if (distance <= kMaxPointsDistance) {
            [locationsCopy addObject:point];
            if (![self.locationsFiltered containsObject:point]) {
                containsAll = NO;
            }
        }
    }
    
    if ((!containsAll || (self.locationsFiltered.count != locationsCopy.count) ) || locationsCopy.count == 0) {
        if ([_locationsFiltered count] > 0) {
            for  (ARGeoCoordinate *coordinate in _arController.coordinates) {
                [coordinate.displayView removeFromSuperview];
            }
            [_arController.coordinates removeAllObjects];
            [_geoLocations removeAllObjects];
        }
        self.locationsFiltered = [locationsCopy copy];
        [self generateGeoLocations];
    }
}

- (void)generateGeoLocations {
    [self setGeoLocations:[NSMutableArray arrayWithCapacity:[_locationsFiltered count]]];
    
    for(AMPoint* point in _locationsFiltered) {
        if (point.isImagePoint){
            ARGeoCoordinate *coordinate = [ARGeoCoordinate coordinateWithLocation:point.arLocation locationTitle:point.name];
            NSLog(@"%@ location: %@", point.name, point.arLocation);
            [coordinate calibrateUsingOrigin:_locationManager.location];
            MarkerView *markerView = [[MarkerView alloc] initWithCoordinate:coordinate forPoint:point delegate:self];
            [coordinate setDisplayView:markerView];
            [_arController addCoordinate:coordinate];
            [_geoLocations addObject:coordinate];
        }
    }
}

#pragma mark - ARLocationDelegate

-(NSMutableArray *)geoLocations {
    if(!_geoLocations) {
        [self generateGeoLocations];
    }
    return _geoLocations;
}

- (void)locationClicked:(ARGeoCoordinate *)coordinate {
    NSLog(@"Tapped location %@", coordinate);
}

#pragma mark - ARDelegate

-(void) didTouchMarkerView:(MarkerView *) markerView{
    
}

-(void)didUpdateHeading:(CLHeading *)newHeading {
    
}

-(void)didUpdateLocation:(CLLocation *)newLocation {
}

-(void)didUpdateOrientation:(UIDeviceOrientation)orientation {
    
}

#pragma mark - ARMarkerDelegate

-(void)didTapMarker:(ARGeoCoordinate *)coordinate {
}



@end
