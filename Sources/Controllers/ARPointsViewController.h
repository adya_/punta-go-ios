#import <UIKit/UIKit.h>

@class AMPoint;

@interface ARPointsViewController : UIViewController

@property (nonatomic, strong) NSArray<AMPoint*>* locations;
@property (nonatomic, strong) NSMutableArray<AMPoint*>* locationsFiltered;
@end
