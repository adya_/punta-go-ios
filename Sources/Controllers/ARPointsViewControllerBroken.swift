import MapKit

let kInfoViewTag = 1001
let kMaxPointsDistance = 200.0

class ARPointsViewControllerBroken : UIViewController {
    @IBOutlet weak var vNav: UIView!
    fileprivate var arController : AugmentedRealityController!
    fileprivate var locationManager : CLLocationManager!
    fileprivate var points : [AMPoint]!
    fileprivate var filtered : [AMPoint]! {
        didSet {
            self.updateLocations()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.points = AMStorage.shared().zones?.flatMap{$0.points}.filter{$0.isImagePoint}
        self.locationManager = self.createLocationManager()
        self.arController = self.createAR()
        self.view.bringSubview(toFront: self.vNav)
    }
    @IBAction func clicked(_ sender: AnyObject) {
        print("Please go back")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.locationManager.startUpdatingLocation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        self.locationManager.stopUpdatingLocation()
    }
    
    func updateLocations() {
        self.arController.coordinates.forEach {
            ($0 as? ARGeoCoordinate)?.displayView.removeFromSuperview()
        }
        self.arController.coordinates.removeAllObjects()
        
        if let coords = self.filtered?.map({ AMPointGeoCoordinate.coordinate(withPoint: $0, fromOrigin: self.locationManager.location)}) {
            coords.forEach {
                $0.displayView = MarkerView(coordinate: $0, for: $0.point, delegate: nil)
                self.arController.addCoordinate($0)
            }
        }
    }

}


// MARK: Orientation Preferences
extension ARPointsViewControllerBroken {
    override var preferredInterfaceOrientationForPresentation : UIInterfaceOrientation {
        return .portrait
    }
    
    override var shouldAutorotate : Bool {
        return true
    }
}

// MARK: - Initialization
extension ARPointsViewControllerBroken {
    func createAR() -> AugmentedRealityController {
        let arController = AugmentedRealityController(view: self.view, parentViewController: self, withDelgate: nil)
        arController?.minimumScaleFactor = 0.5
        arController?.scaleViewsBasedOnDistance = true
        arController?.rotateViewsBasedOnPerspective = true
        arController?.debugMode = false
        return arController!
    }
    
    
    func createLocationManager() -> CLLocationManager {
        let locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        return locationManager
    }
}

// MARK: - CLLocationManager delegate
extension ARPointsViewControllerBroken : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let lastLocation = locations.last!
        let accuracy = lastLocation.horizontalAccuracy
        print("Received location \(lastLocation) with accuracy \(accuracy)")
        let updated = self.points.filter {
            let distance = lastLocation.distance(from: $0.location)
                return distance >= 0 && distance <= kMaxPointsDistance
        }
        if let filtered = self.filtered, updated != filtered {
            self.filtered = updated
        }
    }
}
