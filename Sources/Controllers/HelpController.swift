import UIKit
class HelpController : PortraitViewController {
    
    @IBOutlet weak var tvHelp: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tvHelp.text = AMStorage.shared().help
    }
}
