import UIKit

class LoaderViewController : PortraitViewController {
  
    @IBOutlet weak fileprivate var pvLoading: UIProgressView!
    @IBOutlet weak fileprivate var aiChecking: UIActivityIndicatorView!
    @IBOutlet weak fileprivate var bTryAgain: UIButton!
    @IBOutlet weak fileprivate var lInstructions: UIView!
    @IBOutlet weak var bIdiom: UIButton!
    
    fileprivate var hasData : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AMStorage.shared().delegate = self
        self.showProgress(true)
        if let _ = AMStorage.shared().idiom {
            self.updateIdiom()
            AMStorage.shared().update()
        }
        
        if let zone = AMStorage.shared().zones?.first,
            let file = AMStorage.shared().path(toZoneAudio: zone), !AudioPlayer.shared.playing {
            AudioPlayer.shared.play(file)
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if AMStorage.shared().idiom == nil {
            self.changeIdiom(self.bIdiom)
        }
    }

    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }
    
    @IBAction func tryUpdateStorage(_ sender: UIButton) {
        self.showProgress(true)
        AMStorage.shared().update(true)
    }
    
    @IBAction func changeIdiom(_ sender: UIButton) {
        let alert = UIAlertController(title: "Punta GO", message: "Por favor seleccione el idioma", preferredStyle: .actionSheet)
        AMStorage.shared().availableIdioms.forEach {
            let idiomAction = UIAlertAction(title: $0, style: .default) {
                if AMStorage.shared().idiom != $0.title! {
                    AMStorage.shared().idiom = $0.title!
                    self.updateIdiom()
                    self.showProgress(true)
                    AMStorage.shared().update(true)
                }
            }
            alert.addAction(idiomAction)
        }
        if let popover = alert.popoverPresentationController {
            popover.sourceView = self.aiChecking
            popover.sourceRect = self.aiChecking.bounds
        }
        if AMStorage.shared().idiom != nil {
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(cancel)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func tapped(_ sender: UITapGestureRecognizer) {
        if self.hasData {
            AudioPlayer.shared.stop()
            self.performSegue(withIdentifier: "segProceed", sender: self)
        }
    }
    
    fileprivate func updateIdiom() {
        let idiom = AMStorage.shared().idiom
        self.bIdiom.setTitle("El idioma: \(idiom ?? "")", for: UIControlState())
    }
    
    fileprivate func showProgress(_ indeterminate : Bool) {
        self.lInstructions.isHidden = true
        self.pvLoading.isHidden = indeterminate
        self.pvLoading.progress = 0
        self.aiChecking.isHidden = !indeterminate
        self.bTryAgain.isHidden = true
        self.bIdiom.isEnabled = false
        self.hasData = false
    }
    
    fileprivate func hideProgress(_ retry : Bool) {
        self.lInstructions.isHidden = retry
        self.pvLoading.isHidden = true
        self.aiChecking.isHidden = true
        self.bTryAgain.isHidden = !retry
        self.bIdiom.isEnabled = true
        self.hasData = !retry
    }
    
    fileprivate func updateProgress(_ progress : Float) {
        self.pvLoading.setProgress(progress, animated: true)
    }
    
    fileprivate func showMessage(_ message : String){
        let alert = UIAlertView(title: "Punta GO", message: message, delegate: nil, cancelButtonTitle: "OK")
        alert.alertViewStyle = .default
        alert.show()
    }
    
    @IBAction func unwindHelp(_ segue : UIStoryboardSegue) {}
}

extension LoaderViewController : AMStorageLoadingDelegate {
    func storageWillLoadFiles(_ total: Int) {
        self.showProgress(false)
    }
    
    func storageDidLoadFile(_ filepath: String, for point: AMPoint?, progress: Int, of total: Int) {
        if point == nil && !AudioPlayer.shared.playing { // play zone audio
            AudioPlayer.shared.play(filepath)
        }
        self.updateProgress(Float(progress) / Float(total + 1))
    }
    
    func storageDidFinish(with zones: [AMZone]?, updated isUpdated: Bool) {
        self.hideProgress(false)
    }
    
    func storageDidFailWithError(_ error: String) {
        self.hasData = (AMStorage.shared().zones?.count ?? 0 > 0)
        self.hideProgress(!self.hasData)
        if !self.hasData {
            self.showMessage("Sin conexión! Por favor conecte el equipo a internet e intente nuevamente")
        }
    }
}

