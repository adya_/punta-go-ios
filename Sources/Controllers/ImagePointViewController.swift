class ImagePointViewController : UIViewController {
    private var points : [AMPoint]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.points = AMStorage.sharedStorage().zones?.flatMap{$0.points}.filter{$0.isImagePoint}
        
    }
}

// MARK: Orientation Preferences
extension ImagePointViewController {
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return .Portrait
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
}

// MARK: - Initialization
extension ImagePointViewController {
    func showAR() -> ARViewController {
        let arViewController = ARViewController()
        arViewController.debugEnabled = true
        arViewController.dataSource = self
        arViewController.maxDistance = 0
        arViewController.maxVisibleAnnotations = 100
        arViewController.maxVerticalLevel = 5
        arViewController.headingSmoothingFactor = 0.05
        arViewController.trackingManager.userDistanceFilter = 25
        arViewController.trackingManager.reloadDistanceFilter = 75
        arViewController.setAnnotations(self.points.map {AMPointAnnotation(point: $0)})
        self.presentViewController(arViewController, animated: true, completion: nil)
        return arViewController
    }
}

extension ImagePointViewController : ARDataSource {
    func ar(arViewController: ARViewController, viewForAnnotation: ARAnnotation) -> ARAnnotationView {
        return ImageAnnotationView()
    }
}