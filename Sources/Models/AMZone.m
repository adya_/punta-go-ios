//
//  AMZone.m
//  Around Me
//
//  Created by AdYa on 6/18/16.
//  Copyright © 2016 Jean-Pierre Distler. All rights reserved.
//

#import "AMZone.h"

NSString* const kZoneID = @"ID";
NSString* const kZoneName = @"name";
NSString* const kZoneAudio = @"audio";
NSString* const kPoints = @"points";

@implementation AMZone
-(NSArray*) audioPoints{
    return [self pointsWithFlag:YES];
}

-(NSArray*) imagePoints{
    return [self pointsWithFlag:NO];
}

-(NSArray*) pointsWithFlag:(BOOL) flag{
    return [self.points filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.isAudioPoint == %@", @(flag)]];
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:@(_zoneID) forKey:kZoneID];
    [coder encodeObject:_name forKey:kZoneName];
    [coder encodeObject:_audio forKey:kZoneAudio];
    [coder encodeObject:_points forKey:kPoints];
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [self init];
    if (self) {
        _zoneID = [[coder decodeObjectForKey:kZoneID] intValue];
        _name = [coder decodeObjectForKey:kZoneName];
        _audio = [coder decodeObjectForKey:kZoneAudio];
        _points = [coder decodeObjectForKey:kPoints];
    }
    return self;
}
@end
