enum PointType {
    case audio
    case image
}

private enum Keys : String {
    case PointIDKey = "ID"
    case LocationKey = "location"
    case DistanceKey = "dist"
    case PointNameKey = "name"
    case ImageKey = "image"
    case PointAudioKey = "audio"
    case IsAudioPointKey = "isAudio"

}


struct Point {
    let id : Int
    let name : String
    let image : URL?
    let audio : URL?
    let location : CLLocation
    let markerLocation : CLLocation
    
    var distance : Float
    let type : PointType
    
//    init?(coder aDecoder: NSCoder) {
//        self.id = aDecoder.decodeIntForKey(Keys.PointIDKey.rawValue)!
//        self.location = aDecoder.decodeObjectForKey(Keys.LocationKey.rawValue) as! CLLocation
//        self.distance = aDecoder.decodeFloatForKey(Keys.DistanceKey.rawValue)
//        self.name = aDecoder.decodeObjectForKey(Keys.PointNameKey.rawValue) as! String
//        self.image = aDecoder.decodeObjectForKey(Keys.ImageKey.rawValue) as! NSURL
//        self.audio = aDecoder.decodeObjectForKey(Keys.PointAudioKey.rawValue) as! NSURL
//        self.
//    }
}
