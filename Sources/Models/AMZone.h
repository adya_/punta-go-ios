//
//  AMZone.h
//  Around Me
//
//  Created by AdYa on 6/18/16.
//  Copyright © 2016 Jean-Pierre Distler. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 {
 "zoneID": "1",
 "zoneName": "aaaa",
 "zoneAUDIO": "http:\/\/hideaz.in\/acomp\/audiofiles\/titanic-2303.mp3",
 "points": [
 {
 "pointID":"44",
 "loclat":"-33.683652843788984",
 "loclng":"-53.5529100894928",
 "loclat_2":"-33.683652843788984",
 "loclng_2":"-53.5529100894928",
 "loclat_3":"-33.683652843788984",
 "loclng_3":"-53.5529100894928",
 "Audio_ar":"false",
 "pointAUDIO":"http:\/\/xenioit.com\/xenio\/backend\/acompasadosnew\/\/pointaudiofiles\/44.mp3",
 "pointIMAGE":"http:\/\/xenioit.com\/xenio\/backend\/acompasadosnew\/\/pointimagefiles\/image.jpg",
 "pointName":"tony",
 "distance":"52"
 }
 ]
 },
 */

@class AMPoint;

@interface AMZone : NSObject <NSCoding>

@property int zoneID;
@property NSString *name;
@property NSURL* audio;
@property NSArray<AMPoint*> *points;

@property (readonly) NSArray<AMPoint*>* audioPoints;
@property (readonly) NSArray<AMPoint*>* imagePoints;

@end
