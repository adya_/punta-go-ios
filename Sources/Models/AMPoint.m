//
//  Point.m
//  Around Me
//
//  Created by AdYa on 6/18/16.
//  Copyright © 2016 Jean-Pierre Distler. All rights reserved.
//

#import "AMPoint.h"

NSString* const kPointID = @"ID";
NSString* const kLocation = @"location";
NSString* const kARLocation = @"arLocation";
NSString* const kDistance = @"dist";
NSString* const kPointName = @"name";
NSString* const kImage = @"image";
NSString* const kPointAudio = @"audio";
NSString* const kIsAudioPoint = @"isAudio";

@implementation AMPoint

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [self init];
    if (self) {
        _pointID = [[coder decodeObjectForKey:kPointID] intValue];
        _location = [coder decodeObjectForKey:kLocation];
        _arLocation = [coder decodeObjectForKey:kARLocation];
        _distance = [[coder decodeObjectForKey:kDistance] doubleValue];
        _name = [coder decodeObjectForKey:kPointName];
        _image = [coder decodeObjectForKey:kImage];
        _audio = [coder decodeObjectForKey:kPointAudio];
        _isAudioPoint = [[coder decodeObjectForKey:kIsAudioPoint] boolValue];
    }
    return self;
}

-(BOOL) isImagePoint {
    return !_isAudioPoint;
}

-(void) encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:@(_pointID) forKey:kPointID];
    [aCoder encodeObject:_location forKey:kLocation];
    [aCoder encodeObject:_arLocation forKey:kARLocation];
    [aCoder encodeObject:@(_distance) forKey:kDistance];
    [aCoder encodeObject:_name forKey:kPointName];
    [aCoder encodeObject:_image forKey:kImage];
    [aCoder encodeObject:_audio forKey:kPointAudio];
    [aCoder encodeObject:@(_isAudioPoint) forKey:kIsAudioPoint];
}
@end
