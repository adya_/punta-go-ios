//
//  Point.h
//  Around Me
//
//  Created by AdYa on 6/18/16.
//  Copyright © 2016 Jean-Pierre Distler. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
/*
 {
 "pointID":"44",
 "loclat":"-33.683652843788984",        // Audio location
 "loclng":"-53.5529100894928",          // Audio location
 "loclat_2":"-33.683652843788984",      // AR location
 "loclng_2":"-53.5529100894928",        // AR location
 "loclat_3":"-33.683652843788984",      // ignored
 "loclng_3":"-53.5529100894928",        // ignored
 "Audio_ar":"false",
 "pointAUDIO":"http:\/\/xenioit.com\/xenio\/backend\/acompasadosnew\/\/pointaudiofiles\/44.mp3",        // ignored
 "pointIMAGE":"http:\/\/xenioit.com\/xenio\/backend\/acompasadosnew\/\/pointimagefiles\/image.jpg",
 "pointName":"tony",
 "distance":"52"
 }
 */


@interface AMPoint : NSObject <NSCoding>
@property int pointID;
@property CLLocation* location;
@property CLLocation* arLocation; // location of the AR image.
@property double distance;
@property NSString *name;
@property NSURL* image;
@property NSURL* audio;
@property BOOL isAudioPoint; // Audio_ar flag
@property (readonly) BOOL isImagePoint;
@end
