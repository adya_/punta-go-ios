
let kResumedNotification = "AppResumed"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func applicationDidFinishLaunching(_ application: UIApplication) {
        application.isIdleTimerDisabled = true
 //       TestFairy.begin("c175a27fa89144b1ddec3aaed4c52f35daef7b15")
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        AudioPlayer.shared.pause()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: kResumedNotification), object: nil)
    }
}
